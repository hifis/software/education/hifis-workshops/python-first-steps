<!--
SPDX-FileCopyrightText: 2024 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# First Steps in Python

This repository contains the materials of the workshop [First Steps in Python](https://hifis.net/workshop-materials/python-first-steps/).
On this basis, the official HIFIS workshop website is built.
In addition, instructors can use it to [set up a workshop-specifc pad](setup/README.md).

## Website content and configuration

You require Python >= 3.10 and [MkDocs](https://www.mkdocs.org/) to build the general workshop website.
The reference dependencies are listed in [requirements.txt](requirements.txt) which is generated via [pip-tools](https://github.com/jazzband/pip-tools/).

The website consists of the following content:
- [workshop_materials](workshop_materials) contains the whole website content.
- [custom_theme](custom_theme) contains the configuration of the HIFIS-specific theme.
- [base.yml](base.yml) defines MkDocs options used for all HIFIS workshops.
- [mkdocs.yml](mkdocs.yml) defines the specific MkDocs options for this workshop website.

### Preparation

Please clone this repository and install the required dependencies.
We assume that you have already installed a suitable Python version and that you use a virtual Python environment.

```shell
git clone <GIT REMOTE REPOSITORY URL>
pip install -r requirements.txt
```

### Build the website

You can build the website locally as follows:

```shell
mkdocs build
```

The result can be found in the `sites` directory.

In addition, you can directly serve the website via the integrated development server:

```shell
mkdocs serve
```

### Synchronize with the template

The workshop materials project is based on [this template](https://codebase.helmholtz.cloud/-/ide/project/hifis/software/education/hifis-workshops/cookiecutter-hifis-workshop-template).
You can check for and apply template changes (e.g., a new website layout) with the help of [cruft](https://cruft.github.io/cruft):
- Please run `cruft check` to find out about new template changes.
- Please run `cruft update` to view and apply them.

## Contributors

Here you find the main contributors to the material:

- Fredo Erxleben <f.erxleben@hzdr.de>

## Contributing

Please see [the contribution guidelines](CONTRIBUTING.md) for further information about how to contribute.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content in this repository is licensed.
