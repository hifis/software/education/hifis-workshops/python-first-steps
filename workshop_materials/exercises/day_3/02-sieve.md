<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Sieve of Eratosthenes

<!--
Required Knowledge:

* Constants
* Assignment
  * With fixed value
  * With a calculation
* Output
* While-loop
* For-loop
* Lists
!-->


This is a famous algorithm to find prime numbers that was already known in in the 3rd century BCE. 

Here is how it works:

* You start with an empty list of already known primes.
* Starting with the number 2 and then for all following numbers:
    * Check if the current number can be divided by any of the known primes without remainder
        * If this is the case, the current number can not be prime and you continue with the next number.
        * If all divisions with a preceding prime leave a remainder, the current number is prime as well and is added to the list of known primes.

Use this algorithm to find the 100th prime number.

> [The algorithm on Wikipedia](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes)

/// details | Hints

 [Python's _break_ and _continue_ statements][break-continue] can be of great help in this task.

///

<!-- Links !-->

[break-continue]: https://www.programiz.com/python-programming/break-continue