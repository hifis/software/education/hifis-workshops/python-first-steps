<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# A Random DNA Sequence

<!--
Required knowledge:

* Importing
* Assignment
  * With fixed value
  * With a calculation
* Lists
* Slicing
!-->

**Apologies to all biologists for the extreme simplifications of how DNA works.**

### Creating a Sequence

Generate a random DNA-like sequence made up of the symbols `A`, `C`, `G`, `T`.
The sequence should be 10.000 (ten thousand) characters long.

### Extracting Triplets

Split up the sequence into triplets (i.e. sequences with three characters) store all those triplets in a list.
If some characters get left over, they can be discarded.

### Finding the Good Parts

Go through the list of triplets. 
If you encounter the triplet `AAA` all following triplets go into a separate list (let's say they form the basis for valid genes) until you either encounter `AAA` again, starting a new list or until you reach the triplet `TTT`.
If the list of triplets ends before a gene-list is completed, that gene is discarded.

No gene-list should contain the `AAA` or `TTT` triplets.

Print the amount of found gene-lists, the lengths of each and the total amount of triplets in all genes.

/// details | Hint

Check the `random` module from the _Python_ standard library for a useful function to help you.

///
