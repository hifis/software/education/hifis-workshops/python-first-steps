<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Counting Sheep

<!--
Required Knowledge:

* Assignment
  * With fixed value
  * With a calculation
* Output
* For-loop
* Range
!-->

Write a for-loop that counts sheep from 1 to 20.
In each iteration it should print the number of counted sheep and the number of legs that these sheep have in total (4 per sheep).

**Expected Output**

```
1 sheep, 4 legs
2 sheep, 8 legs
3 sheep, 12 legs
…
20 sheep, 80 legs
```
