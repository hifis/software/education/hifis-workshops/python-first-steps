<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Guitar Hero

The [chromatic scale](https://en.wikipedia.org/wiki/Chromatic_scale) is a common way of denoting music (especially in western cultures).
It is composed of half-tone steps of increasing pitch.
Often when denoting the scale, the note `C` is used as a reference point.
Here are all notes in order (beginning with `C`)

| Half-tone Offset | +0 | +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 | +11 | (+12) |
|------------------|----|----|----|----|----|----|----|----|----|----|----|----|----|
| **Note name**    | C4 | C4# | D4 | D4# | E4 | F4 | F4# | G4 | G4# | A4 | A4# | B4 | C5 |

* The number indicates the octave in which the note lies
    * Each octave contains 12 half-tone steps
    * Octaves increase on every transition from `B` to `C`
    * `C0` is the lowest note humans can hear at around 16.35 Hz
    * `C4` is the reference note for the common _C-major_ scale
* The `#`-character is read as `sharp`
    * It corresponds with the black keys on a piano

/// details | Fun Fact

This is called the [International (or scientific) pitch notation](https://en.wikipedia.org/wiki/Scientific_pitch_notation), the alternative being the [Helmholtz Pitch Notation](https://en.wikipedia.org/wiki/Helmholtz_pitch_notation)

///

### One Note

You probably do want to represent notes as whole numbers, instead of strings for the ease of calculating.
For this you will have to choose which note will be your "0"-note. (In principle this can be any note; Good candidates could be `C0` or `C4`.)

1. Write a function `half_tone_offset(…)` that for a given base note and a given half-tone offset calculates the corresponding note.
2. Devise also a function `note_to_str(…)` that can translate your notes into a string for printing.
    * For later convenience have all notes take up 3 characters, so they align nicely (you can pad them with spaces, for example).

/// note | Example

```
>>> my_note = half_tone_offset(base_note=NOTE_C4, offset=18)
>>> print(note_to_str(my_note))
F5#
>>> my_other_note = half_tone_offset(base_note=NOTE_C4, offset=-1)
>>> print(note_to_str(my_other_note))
B3
```

///

/// details | Hints

* This musical scale repeats itself over and over again. In such cases the `%`-operator (called modulo) usually is a good friend.
* Solve the three components of the printing task separately:
    * What is the base note?
    * Which is the correct octave number?
    * Does it have the `#` sign?
* Defining some constants can go a long way to create more readable and easier to understand code

///

### A Guitar String

On a guitar, each string has a base note that sounds when you pluck it.
Along the neck of the guitar, so-called _frets_ are placed.
When you pinch behind the first _fret_ the string makes a sound one half-tone higher than the base note. If you pinch behind the second _fret_ the sound becomes two half-tones higher, and so on…

Write a function that for a given base note and a given amount of _frets_ prints all the notes along the guitar strings. 

/// note | Example

```
>>> print_guitar_string(base_note=NOTE_E3, frets=15)
E3  | F3  F3# G3  G3# A3  A3# B3  C4  C4# D4  D4# E4  F4  F4# G4 
```

///

### A Guitar Cheat-Sheet

The standard tuning for the six strings of a guitar is `E2`, `A2`, `D3`, `G3`, `B3`, `E4`.

1. Write a function that for a given tuning (i.e. a list of notes) and a given amount of frets prints a guitar cheat sheet. Sort the tuning list beforehand. The strings get printed from the highest pitch on top row to the lowest on the bottom row.

/// note | Example

```
>>> standard_tuning = [NOTE_E2, NOTE_A2, NOTE_D3, NOTE_G3, NOTE_B3, NOTE_E4]
>>> guitar_cheat_sheet(tuning=standard_tuning, frets=4)

E4  | F4  F4# G4  G4#
B3  | C4  C4# D4  D4#
G3  | G3# A3  A3# B3
D3  | D3# E3  F3  F3#
A2  | A2# B2  C3  C3#
E2  | F2  F2# G2  G2#
```

///

2. Include an option to only print non-sharp notes, but keep the arrangement of the other notes. Include a indicator for the frets at the top and bottom to aid orientation (pay attention if your fret numbers go into the double digits!).

/// note | Example

```
>>> guitar_cheat_sheet(tuning=standard_tuning, frets=4, include_sharp=False)

    0   1   2   3   4
----+---+---+---+---+
E4  | F4      G4     
B3  | C4      D4     
G3  |     A3      B3
D3  |     E3  F3     
A2  |     B2  C3     
E2  | F2      G2     
----+---+---+---+---+
    0   1   2   3   4
```

///

3. Optional: Learn to play the guitar. You have the cheat sheet already :)