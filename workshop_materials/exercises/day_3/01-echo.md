<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Echo

<!--
Required Knowledge:

* Assignment
  * With fixed value
  * With a calculation
* Input
* Output
* Loops
* Indexing
* Slicing
!-->

This little program asks the user for input.
It then prints the input that the user has provided.
After that it again prints the input value except for the first letter.
Then it prints the input without the first two letters, and so on until no letters are left.

**Example Output**
```
Which word should I repeat? Echo

Echo
cho
ho
o

```

/// details | Hint

Similar to lists, strings can be indexed and sliced.
The individual elements within a string would then be the individual symbols a string is made of.

**Example:**

/// tab | Code

```python
text = "Hello World"
print(text[6])
print(text[3:8])
```

///

/// tab | Output

```
W
lo Wo
```

///

///
