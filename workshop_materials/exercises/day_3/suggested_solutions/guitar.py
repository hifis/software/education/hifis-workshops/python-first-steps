# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
# SPDX-License-Identifier: MIT

"""
Suggested solution to the guitar exercise
"""

NOTES_PER_OCTAVE = 12

NOTE_C0 = -48
NOTE_E2 = -20
NOTE_A2 = -15
NOTE_D3 = -10
NOTE_G3 = -5
NOTE_B3 = -1
NOTE_C4 = 0  # Standard C
NOTE_E4 = 4

STANDARD_TUNING = [NOTE_E2, NOTE_A2, NOTE_D3, NOTE_G3, NOTE_B3, NOTE_E4]

BASE_OCTAVE = 4

NOTE_BASE_NAMES = [ "C", "C", "D", "D", "E", "F", "F", "G", "G", "A", "A", "B"]

SHARP_OFFSETS = [
    1,  # C#
    3,  # D#
    6,  # F#
    8,  # G#
    10,  # A#
]

def note_name(note: int):
    octave_offset = note // NOTES_PER_OCTAVE
    octave = BASE_OCTAVE + octave_offset
    note_offset = note % NOTES_PER_OCTAVE
    note_name = NOTE_BASE_NAMES[note_offset]
    if note_offset in SHARP_OFFSETS:
        note_name += "#"
    
    return note_name + str(octave)

def notes_on_string(base_note:int, frets:int):
    string_notes = f"{note_name(base_note)} |"
    for note_offset in range(1, frets + 1):
        string_notes += f" {note_name(base_note + note_offset)} "
        
    return string_notes

def guitar_cheat_sheet(tuning:list, frets:int):
    tuning.sort()
    tuning.reverse()
    
    for base_note in tuning:
        print(notes_on_string(base_note, frets))
