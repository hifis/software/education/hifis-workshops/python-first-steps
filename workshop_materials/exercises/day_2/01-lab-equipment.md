<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# The Lab Equipment Dispenser

/// note | About this Task

This task is a bit more complex and designed to familiarize you more with the subtle challenges of real-live programming.

* While the programming structures that you know are perfectly sufficient to solve the task, the ways in which to apply them might not be obvious.
* It is more **complex** than the tasks you have seen before.
Instead of solving it in one go, you may want to solve a part of it, try that out and then continue on the next part.
* It has a **vague specification**, meaning you are not told all details of the problem.
In the cases where you lack information it is up to your best judgement to fill in the blanks.
As long as the program fulfills the specified behaviour, it is considered _correct_.

///

Your lab is is supposed to get a new dispenser for safety equipment.
Management decided that we can develop it cheaper in-house and tasked you with writing the bookkeeping program for it.

Some of your colleagues already went through a brain-storming session and came up with a flowchart that describes how the machine is supposed to work.
They drew you this diagram before they left for a conference:

```mermaid
flowchart TB

    start(( START ))
    stop((( END )))
    should_continue{Continue dispensing?}
    final_stock[Print final stock]

    start --> setup --> dispensing
    should_continue --yes --> dispensing
    should_continue -- no --> final_stock --> stop
    dispensing --> should_continue

    subgraph setup
        direction TB
        set_earplugs(earplugs_in_stock = 10)
        set_goggles(goggles_in_stock = 8)
        set_gloves(gloves_in_stock = 6)
        set_continue(continue_dispensing = True)
    end

    subgraph dispensing
        direction LR
        _start(( START ))
        _stop((( END )))
        has_items{Still has items?}
        print_stock[Print remaining stock]
        get_input[Get user input]
        decide_input{Input is...}
        dispense_gloves[Reduce stock of gloves by 2]
        dispense_goggles[Reduce stock of goggles by 1]
        dispense_earplugs[Reduce stock of earplugs by 2]
        stop_dispensing(continue_dispensing = False)
        _start --> print_stock --> get_input --> decide_input
        decide_input -- gloves --> dispense_gloves --> has_items
        decide_input -- goggles --> dispense_goggles --> has_items
        decide_input -- earplugs --> dispense_earplugs --> has_items
        has_items -- no --> stop_dispensing
        decide_input -- stop --> stop_dispensing
        decide_input -- anything else --> print_stock
        has_items -- yes --> _stop
        stop_dispensing --> _stop
    end
```

Create a program that implements the specified behaviour.


/// details | Hints

Consider using functions to break down the complexity into something more manageable.

///