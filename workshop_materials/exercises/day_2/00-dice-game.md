<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# A Dicey Game

Build a small dice game according to the rules stated below.
The intermediate tasks will guide you along to help build the program step-by-step.

/// note | The Rules

This is a game for 2 players, called `Player 1` and `Player 2`, which each start with a score of 0 points.
It is played with a six-sided die with the numbers 1, …, 6 on it.
The goal is to collect as many points as possible over 5 rounds (or until one player loses) without exceeding a _limit_ in each turn.
This _limit_ usually is set to 21.

In each round, the players take turns.
At the beginning of each turn, the die is rolled three times and the shown numbers are added up to get the _turn sum_.
Then the current player can decide to either

* _Stop_, in which case the _turn sum_ is added to the players score and the next players turn starts.
* _Roll again_, which rolls the die again and adds the number shown to the _turn sum_.
    * Should the _turn sum_ exceed the _limit_ the player was too greedy and loses the game. 

If all rounds have passed and no player has lost yet, the players scores are compared and the player with the higher score wins.
In case the scores are equal, it is considered a draw.

///

## Preparation

If you haven't done so yet, create a new Python file `dice-game.py` in which to put the code for this task.

Create constants as follows:

| Name             | Value        | Comment / Explanation |
|------------------|--------------|-----------------------|
| `PLAYER_1`       | `"Player 1"` | The text used to represent the first player            |
| `PLAYER_2`       | `"Player 2"` | The text used to represent the second player           |
| `DRAW`           | `"Draw"`     | The text used to represent if a draw occurred          |
| `INPUT_ROLL`     | `"roll"`     | The word which the user has to input to roll again     |
| `INPUT_STOP`     | `"stop"`     | The word which the user has to input to end their turn |

## Roll the Dice

To simulate dice rolling, we can use functions from Pythons built-in module `random`.

Read up on the `randrange(…)` and `randint(…)` functions from the module in the [Python Documentation][python-randrange].
Decide which one you want to use to generate a number between 1 and 6 to simulate a dice roll.

Before you write any actual code, test in the REPL if you can import the function of your choice from the `random` module.
Also try calling the function a few time to try out generating random numbers.

Write a function called `roll_dice()` that uses the random functions to simulate a dice roll, prints the rolled number, and also returns the number that was rolled.
(Don't forget the import!)

## Take a Turn

We will wrap the inner workings of a turn into a function, so it can be re-used.

Define a function `take_turn(current_player)`.

It will start by printing which players turn it is.
Next it will prepare a variable `turn_sum` which starts at `0`.
Afterwards it calls the `roll_dice()`-function three times and adds the results to the `turn_sum`.
The `turn_sum` is printed to inform the player about their current amount of points.

Prepare a variable `current_input`, that starts out with _no data_ (yet).
Now, the function enters a loop.
As long as the player did not input the stop-word according to `INPUT_STOP` **or** the `turn_sum` exceeded the `limit`, the following is done:

* Request an input from the player.
* If the input was anything else then `INPUT_ROLL` or `INPUT_STOP`, print which words the player is allowed to use
* If the input was `INPUT_ROLL`, call the `roll_dice()`-function and add the result to the `turn_sum`

After the loop, the function returns the achieved `turn_sum`.

## The complete Game

Define a function `play_dice_game(…)`.

The `limit` a player may achieve in each turn (default: `21`) and the `maximum_rounds` to be played (default: `5`) should be made available as parameters
so users may choose their own rules if they want to do so.

You will need a variables to count how many rounds you have already played and what the total score of each player is.
Also introduce a variable `loser` to keep track if any of the players has lost early.
Of course this initially is `None`.

Create a loop that runs as long as there is no loser **or** the maximum amount of rounds have not been played yet.
Inside, print the current round and print the scores so far.
To play the round, first, `PLAYER_1` takes a turn, then `PLAYER_2` (use the `take_turn(…)`-function accordingly).
If their turn sum did exceed the `limit`, announce that they lost and stop the function immediately.
Otherwise update their `player_…_score`-variables by adding the turn scores onto them.

If the loop completes and no player has lost yet, compare the scores and print the name of the winner or the `DRAW`-text as appropriate.

Run your game by putting

```python
if __name__ == "__main__":
    play_dice_game()
```

at the end of your file.

## Custom Rules

Inside the `if __name__ == "__main__":`-block, ask the user if they want to play with custom rules.
If that is the case, allow them to set a different `limit` and `maximum_rounds`.
Call the `play_dice_game(limit=…, maximum_rounds=…)` with the according values.


<!-- Links !-->
[python-randrange]: https://docs.python.org/3/library/random.html#random.randrange