<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Polygon

<!--
Required Knowledge:

* Assignment
  * With fixed value
  * With a calculation
* Input
* Output
* Importing
!-->

Write a program that can calculate some key values for any **regular** polygon.

## Collecting Information

Ask the user to input the following values:

* `number_of_corners` should be an integer and not less than 3
* `side_length` should be a floating point number larger than 0
  * The value of the `side_length` is assumed to be given in mm

If the input values are not valid, report this in the output.

## Doing the Math

**Do this only if the input was valid.**

From the given values you can infer a lot of other ones.
Calculate the following values:

* The _Center angle_ (in degrees) is the angle between the projections from the center to two adjacent corners.
* The _Corner angle_ (in degrees) is the angle between two adjacent border segments on the inside of the polygon.
* The _Outer radius_ (in mm) is determined by the distance of any corner from the center point.
  * This is the radius of the smallest circle which can completely circumscribe the polygon.
* The _Inner radius_ (in mm) is calculated by the shortest distance fron the center to any border.
  * It happens to be the distance between the center point and the midpoint of any border segment.

![Labeled polygon](./polygon.png)

You may need trigonometric functions from the `math` module for some calculations.
Note that the functions offered work in _radians_ not _degrees_, so you may need to convert between those two.

/// details | Hints

Doing the calculations in some orders is more difficult than in others.
Using half-angles or half-lengths as intermediate values also can make the process a lot easier

Here is one possible approach:

```
center angle = 360° / number of corners
corner angle / 2 = 180° - 90°- center angle / 2
inner radius = tan(corner angle / 2) * (side length / 2)
outer radius = sqrt(inner radius² + (side length / 2)²)
```

Of course you can simplify those formulae and have to still take care of the whole _radians_ ←→ _degrees_ conversion.

///
