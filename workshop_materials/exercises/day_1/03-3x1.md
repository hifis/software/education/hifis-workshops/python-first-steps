<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# The Collatz-Conjecture

<!--
Required Knowledge:

* Assignment
  * With fixed value
  * With a calculation
* Input
* Output
* While-Loop
* Conditionals
  * If-Else
  * Formulating Conditions
!-->

## The Problem

This is a famous and not yet fully solved problem in mathematics. Here are the rules:

* Start with any **positive integer** number _x_
* If _x_ is even, assign _x ← x/2_
* If _x_ is odd, assign _x ← 3x + 1_

Eventually your sequence will end up in the loop 4 → 2 → 1 → 4 …
(The unsolved part is that there is no proof yet that this always happens.)

## Now you!

Implement a loop that generates and prints the sequence for any given positive starting number.
You can either set the starting value in a variable at the beginning of your program or read it from a user input.
The loop should stop if _x_ reaches one of the three numbers (4, 2, or 1) that are known to form an endless cycle.

**Example Output:**

```
Starting Value: 5
16
8
4 - 2- 1, Yeah, I know...
```

/// details | Hint

To check if a number is even, the modulo-operator (written as `%` in Python) might be very helpful.
It calculates the remainder of a whole-number division.

For example `13 // 3 == 4`and since `4 * 3 == 12` that leaves a reminder of `1` to get to the original `13`.
Therefore, `13 % 3 == 1`.

///
