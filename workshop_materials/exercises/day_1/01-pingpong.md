<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Ping-Pong

<!--
Required Knowledge:

* Input
* Output
* While-Loop
* Conditionals
  * If-Elif-Else
  * Formulating Conditions

!-->

## Ping or Pong

Write a program that reads the users' input **once**.
Depending on the input given, it should do different things:

| Input         | Reaction                       |
|---------------|--------------------------------|
| `ping`        | Output `pong`                  |
| `pong`        | Output `ping`                  |
| empty string  | Do nothing (No Output)         |
| anything else | Output `That is not Ping-Pong` |

Consider first what an _empty string_ could mean and how you might check for it.

/// details | Hints

If you are unsure about the empty string try the following in the REPL:

```python-repl
>>> user_input = input("Hit Enter, nothing else")
>>> user_input
```

_Doing nothing_ turns out to be harder that it sounds.
You have some options to approach this:

1. Learn about the `pass` keywoard
2. Organize your conditions in a way, that the _empty string_ case would end up in the `else` branch and leave that out.
3. Consider nesting two conditionals into each other with the outer one checking if whether the input is am empty string.

///

### A whole Match

Put your code inside a loop to run it repeatedly.
The loop should end if the user inputs the word `stop`.
