<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Countdown

<!--
Required Knowledge:

* Assignment
  * With fixed value
  * With a calculation
* Output
* While-Loop
* Conditionals
  * If-Elif-Else
  * Formulating Conditions
!-->

## A simple Countdown

Write a program to output a simple countdown from 10 to 0.
Start with a variable to hold your `current_number` and use a `while`-loop to deal with the printing and updating the number.

/// details | Hints

Consider the following:

* What should your starting value for the `current_number` be?
* What condition should be fulfilled to continue the loop?
* How do you increase the value in the `current_number` variable?
* Should you first print the `current_value` and count it up afterwards or the other way around?

///

/// details | Expected Output

```
10
9
8
7
6
5
4
3
2
1
0
```

///

### Starting the Engine

When the countdown has reached 0, also output the word `Liftoff`.

/// details | Expected Output

```
10
9
8
7
6
5
4
3
2
1
0 Liftoff
```

///

## A less simple Countdown

Write a programm that again implements the countdown from 10 down to 0.
This time, there are some additional rules:

* After reaching the number 6, it shall output `Starting engine` once.
* After reaching the number 4, it shall output the words `and counting` after each number, 
  * Except for the number 0, which is instead followed by `Liftoff` instead.

/// details | Expected Output

```
10
9
8
7
6 Starting engine
5
4 and counting
3 and counting
2 and counting
1 and counting
0 Liftoff
```

///
