<!--
SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
SPDX-FileCopyrightText: 2024 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# First Steps in Python

This is the template for HIFIS workshop materials. 
Here you can briefly describe what this workshop is about.

<!-- Keep this note to indicate where currently offered workshops can be found -->
/// details | Looking for a currently offered workshop?
    type: tip

The workshop is regularly offered by the [HIFIS team](https://hifis.net/).
Please see the [course catalog](https://www.helmholtz-hida.de/course-catalog/en/?search%5Bfilter%5D%5B0%5D=tags%3AHIFIS) for current offerings.
If you are affiliated with Helmholtz and would like to have a dedicated workshop for your team, please [contact us](https://hifis.net/contact.html).

///

## Audience

This workshop is intended for learners who have no prior experience in Python or programming in general.

## Requirements

You will need a Python programming envorinment to be installed.
In case you do not have one or don't know which one to choose,
we recommend [Thonny](https://thonny.org/) as a good beginner tool.

## Structure

### Episodes

The content is split into thematic episodes which are ordered along a story arc.
Each episode contains a code checkpoint at the end to help learners to get back on track if they get lost or something went wrong when experimenting around.

### Tasks

The workshop features some beginner-friendly tasks starting as very small warm-up exercises and then guide towards building more complex programs.
There are no official solutions. 
A task counts as solved, when a give program adheres to all specifications made by the task.
If you would like to receive guidance on solving these tasks or a review of your solutions please reach out via the contact given below and we will be happy to help you along.

### Resources

Additional materials that are more formal and factual and did not directly fit into the course but are _Good to know_ can be found in the resources section.
They are sorted loosely by topic to collect the most fundamental information that beginners tend to look up the most.


## Contact

For any inquiries about this workshop, please [contact us](https://hifis.net/contact.html).
