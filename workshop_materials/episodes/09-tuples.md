<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Tuples

## One Day in a nice Package

Let's put all the code to simulate one day into its own function as well, in preparation for simulating multiple days later on.
To simulate a day, we need the population and food available at the beginning of the day and the function then should return the population and remaining food at the end of the day.
We will do the adding of food between the days, so it should not be part of the function.

Our new function definition can also be put into its own file, just to get it out of the way a bit.

So let's create a new file `simulation.py` in which we put our new function.
The majority of the required code, we have already figured out in the `__main__.py` so we can move it over from there.
Also note that we do no longer need to import the constants in the `__main__.py` since they are needed in the `simulation.py`

/// tab | `simulation.py`

```python
from constants import INDIVIDUALS_PER_GROUP, FOOD_PER_INDIVIDUAL, GROWTH_PER_GROUP, DECLINE_PER_GROUP

def simulate_day(initial_population, initial_food):  # See note (2)(3)
    """Simulate the passing of one day.

    Don't forget to feed the aliens between two simulation steps!

    Args:
        initial_population:
            The population at the beginning of the day.
        initial_food:
            The amount of food available at the beginning of the day.
    Returns:
        A 2-element tuple containing the population at the end of the day 
        and the remaining food at the end of the day.
    """
    excess_food = initial_food - initial_population * FOOD_PER_INDIVIDUAL
    number_groups = initial_population // INDIVIDUALS_PER_GROUP

    will_grow = excess_food > 0
    will_shrink = excess_food < 0

    # Calculate the population at the end of the day
    if will_grow:
        new_population = initial_population + number_groups * GROWTH_PER_GROUP
    elif will_shrink:
        new_population = initial_population - number_groups * DECLINE_PER_GROUP
    else: 
        new_population = initial_population

    # Deal with negative excess food, since we can not carry over missing food to the next day
    if excess_food < 0:
        excess_food = 0

    return (new_population, excess_food)  # See note (1)
```

Something interesting is going on here:
1. To return multiple values at once we packed them into a _tuple_.
2. Instead of using the variables `current_population` and `current_food` here, we instead named the parameters `initial_population` and `initial_food` as well as introducing the variable `new_population` to clearer communicate their purpose.
3. The `print(…)` function calls were also removed to reduce the clutter. We let the `__main__` module print instead at the beginning and end of the day.

///

/// tab | `__main__.py`

```python
from parameters import input_positive_integer
from simulation import simulate_day

print("The Alien Growth Model")

starting_population = input_positive_integer("starting population")
food_per_day = input_positive_integer("amount of food per day")

# Variables to carry information over from one day to the other
current_population = starting_population
current_food = food_per_day  

print("Population at the start of day:", current_population)
print("Food at the start of day:", current_food)

(current_population, current_food) = simulate_day(current_population, current_food)

print("Population at the end of day:", current_population)
print("Food at the end of day:", current_food)

# Feed the aliens between the days
current_food = current_food + food_per_day

```

///

## What is a Tuple?

A tuple is a data type that can hold multiple distinct elements. 
There are more data types of this kind with slight differences regarding their properties and behaviour.
You can refer to the [data types overview][data-types] for all the details.

With tuples the individual values inside it can be accessed by an index.

```python
current_population = starting_population
current_food = food_per_day  # Don't forget to feed them on the first day 

first_day_result = simulate_day(current_population, current_food)
current_population = first_day_result[0]
current_food = first_day_result[1]
```

Alternatively, Python is clever enough to match the individual elements if the variable used to store the result is a tuple as well.
This is called _unpacking_.

```python
current_population = starting_population
current_food = food_per_day  # Don't forget to feed them on the first day 

(current_population, current_food) = simulate_day(current_population, current_food)  # This will assign both variables at once
```

<!-- TODO this will require a sketch to support the mental image !-->

/// note | Key Points

* Tuples can be used to pack multiple values together in one variable.
* The individual elements can be accessed by an index, which starts counting from 0.
* To take the tuple apart, _unpacking_ is also an option.

///

/// details | Code Checkpoint

This is the code that we have so far:

* 📁 `alien_growth_model`
    * [`__main__.py`](../code_checkpoints/09-tuples/__main__.py)
    * [`constants.py`](../code_checkpoints/09-tuples/constants.py)
    * [`parameters.py`](../code_checkpoints/09-tuples/parameters.py)
    * [`simulation.py`](../code_checkpoints/09-tuples/simulation.py)

///

<!-- Links !-->

[data-types]: ../resources/data_types_overview.md