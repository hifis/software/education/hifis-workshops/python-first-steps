<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Getting Input

## Let's ask the User

We do not always want to adapt our code when we try to run a different scenario.
Instead we could let the user input the desired starting values.

To do so, we can use the built-in `input(…)`-function.
It takes one argument, the so-called _prompt_, which is the text that asks the user to type in something. 

Lets start by modifying how we set up the initial population in `parameters.py`:

```python
starting_population = input("Please input the size of the starting population: ")
```

/// note | How did that happen?

You may wonder how it comes that the `input(…)` function was used even though it is not in the `__main__.py`.
The full truth is that, whenever you import a module (which we do at the beginning of the `__main__` module), the whole module gets read and any code in it is executed.
This is also the reason why _Python_ knows about the variables and constants we imported: It has already seen them!

Usually, placing code to be executed in a imported file is not the best practise.
We will improve this approach once we learn about functions.

///

Once the program reaches that line, it will stop and wait for the user to type something in and press the _Enter_-key.
If we try that out now, our prorgam will eventually run into some errors.
The reason is, that the `input(…)`-function will hand over the what the user typed in as a _string_ even though we want to have an _integer_ for the starting population.

To remedy this issue we will have to use the _type casting_ that we mentioned earlier.
Also, let's introduce a new variable `user_input` to hold the raw input as text, to make it clear what is going on step-by-step. 
Our fixed code looks like this:

```python
user_input = input("Please input the size of the starting population: ")
starting_population = int(user_input)
```

We now have a working code again and it has become more flexible through our change.
But sometimes, users do not understand correctly what is asked of them or may not know all of the rules their input has to satisfy.
In our case, someone might get the idea to input a negative number or `0`.

We can use a condition to check for that.

```python
user_input = input("Please input the size of the starting population: ")
input_as_int = int(user_input)  # A new variable, since we do not know if this is a valid starting population yet
if input_as_int >= 0:  # That is valid
    starting_population = input_as_int
else:
    # Try again?
```

## Can you repeat that, please?

The _try again_ part is tricky.
To repeat a section of code, we can use a _loop_.

/// note | Loops in Python

In Python, loops come in two flavors:

1. The `for`-loop repeats a set amount of times
2. The `while`-loop repeats as long as a condition is met.

///

How often should we try again?
Naturally, we would assume that we repeat the process until we have a valid input, which makes it an excellent case for a _while_ -loop.

For our use-case we will have to repeat the whole _input_-_check validity_-_try again_ cycle until we have a valid input.
Also we will add the `input_is_valid` variable to keep track of whether the input made by the user fulfills our criteria or not and whether we should repeat the loop.

Since we need the variable in the beginning of the loop but only get our input inside the loop, it's initial value might be a bit tricky.
We basically need a value to say _unknown_ or _no data_.
In Python we can use the value `None` for exactly this purpose.
When used in a condition, `None` behaves the same as the `False` value.

```python
# We need the variable but we don't actually know it's real value yet
input_is_valid = None

while not input_is_valid:
    user_input = input("Please input the size of the starting population: ")
    input_as_int = int(user_input)
    input_is_valid = input_as_int > 0  # Re-evaluate the validity
    if not input_is_valid:
        print("Sorry,", input_as_int, "is not a valid input, please try again")

# We are now outside the loop (no longer indented)
# The input_as_int should be valid now
starting_population = input_as_int
```

Now the user will repeatedly be asked to try again until they input a number larger than 0.

/// note | Key Points

* The `input(…)`-function can be used to query the user for information
* Remember to type cast the given input accordingly.
* Loops can be used to repeat sections of code
* A `while`-loop repeats as long as a condition is met.

///

/// details | Code Checkpoint

This is the code that we have so far:

* 📁 `alien_growth_model`
    * [`__main__.py`](../code_checkpoints/07-while-input/__main__.py)
    * [`constants.py`](../code_checkpoints/07-while-input/constants.py)
    * [`parameters.py`](../code_checkpoints/07-while-input/parameters.py)

///

/// warning | What have you learned so far?

This would be a good time to apply what you have learned with a first set of exercises.

Try your hand on

* [Creating a Countdown](../exercises/day_1/00-countdown.md)
* [Playing Ping-Pong](../exercises/day_1/01-pingpong.md)
* [Doing some Geometry](../exercises/day_1/02-polygon.md)
* [solving a number problem](../exercises/day_1/03-3x1.md)

///