<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Functions

## Little Building Blocks

We have figured out how to enable the user to input one parameter of our simulation and also how to make sure that the input value is a positive number.
If we now want to do this for the other parameters as well, we might think of copying the code and adapting it a bit.
This is however not a good idea.

/// warning | Dangers of Copy & Paste-Code

Code that is repeated bears the risk of making your program harder to read and maintain.
Some common issues that come along with duplicating code are:

* Forgetting to adapt a part of the copied code
* Finding all copied segments can be tricky, especially when they need to be updated to changed requirements
* If the copied code has a subtle bug, the same bug will be replicated multiple times in your code and you need to make sure find all occurences

///

To circumvent the issues that arise from copying code and to create smaller, self-contained segments, that can be re-used, a `function` is the way to go.
You already encountered functions like `print(…)` and `input(…)`.
Now it is time to write our own.

We will start by providing a _function definition_.
This will explain to our computer what this function is about, what its _function name_ is and which _parameters_ have to be provided, so it has all required information to do its work. 
The function definition must be processed by Python can use the function, so very often functions either are defined at the beginning of a file or even in a separate module that gets imported at the beginning.

Let's begin by writing a function that can request the `starting population` from the user.
We can put this at the beginning of the `parameters.py` file.

```python
def input_positive_integer():  # See note (1)(2)(3)(4)
    input_is_valid = None
    while not input_is_valid:
        user_input = input("Please input the size of the starting population: ")
        input_as_int = int(user_input)
        input_is_valid = input_as_int > 0
        if not input_is_valid:
            print("Sorry,", input_as_int, "is not a valid input, please try again")

    return input_as_int  # See note (5)
```

Let's take a closer look at the notes:

1. The `def`-keyword tells Python that the following is a _function definition_.
2. The _function name_ is given as `input_positive_integer`.
3. The _parameters_ are listed in parenthesis `(` `)`. Even if there are no parameters given in this case, the parenthesis **must** be there.
4. `def` + _function name_ + _parameters_ form the so-called _function head_, which is concluded by a colon `:`
5. After the function has finished, all variables used inside are lost. To get a result out of the function, the `return`-keyword allows to hand back a value that can be assigned to a variable again. Using the `return`-keyword stops the function right then and there and hands back the value of whatever you return. 

### What does this do?

Within pairs of three quotes (`""" … """`) we can add a _documentation comment_ (also known as _docstring_) to give the reader of the code an idea what this function is about.

/// note | Documentation

Code is more often read than written and thus should be easy to grasp and well-documented.
The _docstring_ may be omitted in very trivial and obvious cases.
If in any doubt: Add a documentation.

///

```python
def input_positive_integer():
    """Query the user to input a positive number.

    If the given number was negative, the user is prompted to try again.

    Returns:
        A positive integer number as given by user input.
    """
    …  # Function body as before
```

## More customization

This current function always asks for the starting population, which is a bit unlucky.
We would like to be able to customize the function a bit so it can ask for different things.
For this purpose we can add a _parameter_ to the function.

```python
def input_positive_integer(ask_for_what):  # see note (1)(2)
    """Query the user to input a positive number.

    If the given number was negative or 0, the user is prompted to try again.
    
    Args:
        ask_for_what:
            A text indicating what the user should input the value for.
            It will be used for constructing the prompt that the user
            sees when asked for the input.
    Returns:
        A positive number as given by user input.
    """
    prompt = "Please input the " + ask_for_what + ": "  # See note (3)
    … # Loop as before
        user_input = input(prompt)  # See note (3)
    … # Rest as before
```

1. We added a parameter called `ask_for_what` which we can use later to specify the text that should be included when asking for input.
2. Note that when we add a new parameter to the function it also has to be documented.
3. To make the line for the input request a bit less convoluted we also introduced a variable `prompt` in which we store the full text of the request that the user gets to see.


## Time to Shine!

With our function defined, we can use it.
To do so we write down its name, followed by parenthesis.
In the parenthesis we provide the values for the parameters.

Instead of providing a fixed value, we can now use the function to get the parameters right in the `__main__.py`:

```python
from parameters import input_positive_integer  # See note (1)

# Note that we do an assignment here to capture the values returned by the function in the respective variables
starting_population = input_positive_integer("starting population")  # See note (2)
food_per_day = input_positive_integer("amount of food per day")
```

1. When importing a function you need only its _name_, no parenthesis or parameters are required in this case.
2. If not specified otherwise, Python will assign the values to the parameters in the order they are given.

/// note | The fine print

Using a function is referred to as _calling_ a function.
A function must be _defined_ before it can be used.
You very often find function definitions at the start of files or in separate files (we will talk about how to do this later).

///

/// note | Key Points

* _Functions_ can be used to split a program into smaller, independent chunks
* Functions have a _function name_ and _parameters_ in parenthesis
* Functions must be _defined_ before they can be _called_

///

/// details | Code Checkpoint

This is the code that we have so far:

* 📁 `alien_growth_model`
    * [`__main__.py`](../code_checkpoints/08-functions/__main__.py)
    * [`constants.py`](../code_checkpoints/08-functions/constants.py)
    * [`parameters.py`](../code_checkpoints/08-functions/parameters.py)

///

/// warning | What do you know now?

You can use your newly gained knowledge to try and write some more interesting programs:

* [Play a dice game](../exercises/day_2/00-dice-game.md)
* [Program a lab equipment dispenser](../exercises/day_2/01-lab-equipment.md)

///
