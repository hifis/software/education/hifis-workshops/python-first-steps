<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Conditionals

## Is That So?

Now that can correctly determine how many groups our alien population will form, we can take a look on how the food situation develops.
There are three possible options:

* The population will grow
* The population will be stable
* The population will shrink

When we have such statements, what we are interested in is the _truth_ of it (i.e. Is this statement `True` or `False`).
It is possible to express these statements in Python.

```python
will_grow = excess_food > 0
will_shrink = excess_food < 0
```

> **Note:** it is sufficient to express the two "interesting" statements here, should both turn out to be `False`, it has to be the third option.

The data type that our two variables `will_grow` and `will_shrink` employ is called a _boolean_ (`bool` for short).

### Logically…

While boolean logic forms its own branch of mathmatics, it is sufficient to understand a few basic ideas.
It only allows the two values: `True` and `False`.
Instead of your regular mathmatical operators you have

not
: Inverts the truth value, so `not True` becomes `False` and vice versa.

and
: Combines two values and decides whether _both_ are `True`.

or
: Combines two values and decides whether _at least one_ of them is `True`

## Checking Conditions

In Python we can use a boolean value to make a decision.

```python
from constants import …, GROWTH_PER_GROUP

…

if will_grow:
    current_population = current_population + number_groups * GROWTH_PER_GROUP
    print("Population grows to", current_population, "individuals")
```

The `if`-keyword initiates the checking of a following _condition_.
Any action that should be taken if the contition is `True` is written in the following lines, also known as a _branch_.
This section is indented by one level ( = 4 spaces) to signify that it belongs to the `if` above.

### What else?

Now we have also a second condition that we have to check, because there are still two alternatives left

```python
from constants import …, GROWTH_PER_GROUP, DECLINE_PER_GROUP

if will_grow:
    current_population = current_population + number_groups * GROWTH_PER_GROUP
    print("Population grows to", current_population, "individuals")
else:
    if will_shrink:
        current_population = current_population - number_groups * DECLINE_PER_GROUP
        print("Population shrinks to", current_population, "individuals")
    else:
        print("Population is stable at", current_population, "individuals")
```

The `else`-keyword allows us to specify a branch with an alternative should the original condition not have been met.
We can also nest these conditionals inside each other as often as we like, however it can become very unwieldy very quick.
There is a better way to write it down though.

```python
if will_grow:
    current_population = current_population + number_groups * GROWTH_PER_GROUP
    print("Population grows to", current_population, "individuals")
elif will_shrink:
    current_population = current_population - number_groups * DECLINE_PER_GROUP
    print("Population shrinks to", current_population, "individuals")
else:
    print("Population is stable at", current_population, "individuals")
```

It is allowed to contract an `else` with a following `if` into the `elif`-keyword, which allows to specify a secondary condition.
You can have as many `elif`-segments as you like, but there are some rules to keep in mind:

* The `if` always comes first.
* The `else` always comes last.
    * If there is nothing to do in the `else`-branch, it can be ommitted.
* `if` and `elif` get checked in the order they are written down.
* The first condition that was `True` decides which branch gets evaluated.
    * All following conditions and blocks are then ignored.
    * Should no condition be `True`, the `else`-branch is evaluated.

/// note | Now you

We must not forget to adjust the food count for the next day.
If the excess food was negative, we can not make that up retroactively :(
But luckily the food does not go bad that fast, so if there is any left over we can leave it for the next day.

Assign the value of `food_per_day` to the variable called `current_food`.
If there was excess food left over, add it to the `current_food` as well.

///

/// details | Spoilers

```python
…

current_food = food_per_day
if excess_food > 0:
    current_food = current_food + excess_food
```

Note that there are other correct solutions!

///

Now we have all we need to simulate one day of our population 😀

/// note | Key Points

* A _condition_ can be checked with an `if`-clause
    * A _branch_ is an indented section of code following an `if`, `elif` or `else`
    * Should the condition be `True` the _branch_ after it will be executed
    * Otherwise, the branch will be skipped
* Alternatives are specified with an optional `else`-clause
* An `else` and a following `if` can be combined into an `elif` for better code structure
* The clauses always follow the order `if` - `elif` - `else`

///

/// details | Code Checkpoint

This is the code that we have so far:

* 📁 `alien_growth_model`
    * [`__main__.py`](../code_checkpoints/06-conditionals/__main__.py)
    * [`constants.py`](../code_checkpoints/06-conditionals/constants.py)
    * [`parameters.py`](../code_checkpoints/06-conditionals/parameters.py)


///