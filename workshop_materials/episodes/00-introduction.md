<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Introduction

## Content

This course is a fundamental introduction to programming in general and to the programming language _Python_ in particular.
Basic concepts like

* Variables
* Data Types
* Loops and Conditionals
* Functions

will be covered.
Further there are additional exercises to check the learning progress and provide opportunities to gather experience.

## Audience and Prerequisites

This course is intended for learners who have never programmed before or want to get a basic introduction into the _Python_ programming language.
A fundamental understanding of basic mathematics and how to operate a computer is required.

While there are multiple ways to install _Python_ and interact with it, the tool [thonny][link-thonny] is likely the easiest to install and use for beginners.
It brings everything that is required in one compact bundle and is not overburdened with expert features.

## Getting started

Once you have the programming software of your choice set up and ready to go, you can start by taking a look at the next episode with the button below to learn about the mission that will form the narrative for the course.

### A Note on Conventions

The Python programming language does a lot of things by convention rather then strictly enforcing them.
If you know what you are doing and have a good enough reason to circumvent these conventions you are allowed to do so, however if possible it is strongly recommended to stick to them if at all possible.
This tutorial will stick closely to the established conventions to also convey a best-practise approach.
In most cases, it will not dive into the rationale behind them to keep focussed on the main content.
Very often specific reasoning can be found in the [_Python Enhancement Proposals_ (PEP)][pep] if more details are desired.

<!-- Links !-->

[link-thonny]: https://thonny.org/
[pep]: https://peps.python.org/

