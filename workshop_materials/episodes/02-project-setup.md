<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Setting up a small Project"

## Organization is Everything

When starting an new project, be it programming or otherwise, it is a good idea to first create a new folder in which all files for the project are placed.
In the following, this will be called the _top-level_ or _root folder_.

Create a new folder in a location where you prefer to have your projects (e.g. your desktop, or documents or home folder…) and call it `alien_growth_model`.
In this folder we will put all our files for this example project.

### The main thing

Python code can be written into one or more files and organized in so called [_modules_][python-modules].
Python files end with `.py`.

Each program, be it in one or more files, will need a well-defined starting point.
By convention the file containing this starting point is called `__main__.py`.

This could be your current structure now:

```
📁 alien_growth_model
    └─ main.py
```

/// note | The double underscores `__`

Surrounding the name of something (files, code elements, …) with double underscores `__` usually holds a special meaning:
This is important for the internal working of Python itself. 

You should not lightly name things with `__` and be extra careful when using these things.

///

With the project folder and the `__main__.py` file set up, we are ready to get started.

/// note | Mini-Programs

Sometimes, all you want to do is to have a small file with a few lines of code in it instead of a whole project.
These files are called _scripts_ and instead of a project folder on their own, most developers tend to sort them into a collective folder or put them next to the data files the program works with.
These scripts then also tend to get a more descriptive name, like `calculate_polygon_circumference.py`

///

## Interacting with Python

To run any _Python_ code on your computer you need to use an _interpreter program_.
It translates the code into the machine language that your computer uses and also translates the answers back into something you can read.
The _interpreter program_ is also called _python_.

The _Python interpreter_ can be used in two ways:

1. Writing a file with code in it, which then gets processed by the _interpreter_.
2. Entering an interactive session (called REPL) with the _interpreter_ and issuing the commands one-by-one with direct feedback.

In practise you will often use the first option for code you intend to keep and the second to experiment around.
Many tools also allow you to also continue with an REPL after procesing a file.

### Way 1: Running Python from a file

To write a Python file, any plain-text editor is sufficient (although having a dedicated tool for Python is extremely helpful).
Python files themselves are plain text files with the ending `.py`.
To run them, most tools have a dedicated button, that looks like a _play_-button.
Alternatively you can pass them into the python command on the command line.

/// tab | Run a project

Assuming your project folder is called `my_project` and has a `__main__.py` file inside:

```sh
python my_project
```

or, in slightly older versions of _python_:

```sh
python -m my_project
```

///
/// tab | Run a single file

Assuming you have file called `my_program.py`:

```sh
python my_program.py
```

///

Don't forget to save any changes before running the file or Python will not see the current version.


### Way 2: REPL

The abbreviation stands for **R**ead user input, **E**valuate, **P**rint result, **L**oop to the first step.
It describes an interaction that feels like an text-chat with a computer.

Many tools offer a Python console window for that purpose or you can run the command `python` (without any added parameters) on the command line.

/// note | ipython

There is also a tool called [ipython][ipython] available for the command line which is quite more comfortable than the regular `python` command when doing REPL.

///

/// note | Now you!

Here is a very first Python program to try out:

```python
print("Hello World!")
```

Try to run it as a REPL and by writing it into a file called `hello_world.py` and running the file.

///


## Starting the Work on our Program

For our current project we will start by creating a project folder called `alien_growth_model`.
Inside we add the file `__main__.py`.
Let's also add a line of code inside this main file so we can see if everything is working as intended:

```python
print("The Alien Growth Model")
```

We will learn the details of how this works later on.
For now, we need to understand that `print(…)` is a function that outputs a given value on the screen.
The `"` around `The Alien Growth Model` denotes literal text as opposed to something that might be program code.

/// note | Now you

Try to run the project in your `python` interpreter.
If you are using the command line, pay attention that your working directory should be the one above your project folder. 

///

---

/// note | Key Points

* Python code can either be written in files or used interactively in a REPL
* Python files are plain text and end in `.py`
* Writing code in files is good for storing and sharing code
* The REPL is practical for experimentation
* Python projects tend to be in their own folder and have a `__main__.py` file that acts as the starting point

///


/// details | Code Checkpoint

This is the code that we have so far:

* 📁 `alien_growth_model`
    * [`__main__.py`](../code_checkpoints/02-project-setup/__main__.py)
///

<!----- LINKS ----->
[ipython]: https://ipython.org/
[python-main]: https://docs.python.org/3/library/__main__.html
[python-modules]: https://docs.python.org/3/tutorial/modules.html

