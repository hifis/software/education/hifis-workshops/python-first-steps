<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Our Mission

## A Great Discovery

During one of our recent space explorations, we have discovered the existence of non-sentient alien lifeforms!
Our mission team has already made some initial observations about their behaviour.

### Initial Observations

All time is measured in (earth) days.

* Each day they gather in groups of seven to feed and reproduce
* Excess individuals can not form a group.
    * Any population with less than seven members therefore can not sustain itself.
* At the beginning of the day, each individual eats 0.25 kg of food.
* Afterwards, they will produce three offspring per group per day if excess food is available.
* If the food is insufficient, two of the group will perish each day.
* The rates of population change seem not to be influenced by the amount of food surplus or shortage
* At the end of a day these groups get dissolved, and the next day starts with new groups of seven again.

### Your Mission

Our task is to build a growth model simulation that can predict the population of our new (hopefully) friends from a given population and static food supply.


