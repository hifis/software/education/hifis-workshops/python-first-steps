<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Importing Modules

## Keeping Organized

We already have several parts in our code that we could split off, to keep them out of the way of the other work we want to do.
One example could be to separate the constant values and the starting parameters so they don't get into the way of the other code to come.

Let's do this by creating two more files in our project:

```
📁 alien_growth_model
 |- __main__.py
 |- constants.py  (new)
 |- parameters.py (new)
```

We cut out the constants section from the `__main__.py` file and put it into `constants.py`.
Further we move the starting parameters section into `parameters.py`.
This is how the files look now:

/// tab | `__main__.py`

```python
print("The Alien Growth Model")

current_population = starting_population
current_food = food_per_day  # Don't forget to feed them on the first day
excess_food = current_food - current_population * FOOD_PER_INDIVIDUAL
number_groups = current_population // INDIVIDUALS_PER_GROUP

print(current_population, "individuals,", current_food, "food, leaves", excess_food, "food")
print("There are", number_groups, "groups")
```

///

/// tab | `constants.py`

```python
# Constant values, independent of starting parameters

# How many individuals are required to form a group
INDIVIDUALS_PER_GROUP = 7

# Number of individuals that will be born (per group) if excess food is available
GROWTH_PER_GROUP = 3

# Number of individuals that will perish (per group) if food is insufficient
DECLINE_PER_GROUP = 2

# Amount of food (in kg) that each individual requires
FOOD_PER_INDIVIDUAL = 0.25
```

///

/// tab | `parameters.py`

```python
# Simulation starting parameters

# The amount of individuals at the begin of the simulation
starting_population = 118

# The amount of food available for each day
food_per_day = 20
```

///

If we try to run our program now, _Python_ will give us some errors that some names in our main program no longer are found - which is to be expected, since we just moved them.

## It's over there!

To make _Python_ aware that there is another file with code in it, that it needs to read, before it can do all the other work, we need to _import_ those files.
Let's say we want to print the `INDIVIDUALS_PER_GROUP` from the `constants.py`.
We have two general ways to achieve this:

/// tab | Module Import

### Importing a whole Module

We could import the whole module which would then force us to add the module name in front of everything that we want to use.

```python
# Note: we only use the module name, which does not include the .py file ending
import constants

print(constants.INDIVIDUALS_PER_GROUP)
```

/// note | The `.` operator

The `.` is very common in _Python_.
It comes from the concept of _Object-oriented-programming_.
Without going into details, you can read a pattern like `a.b` as _use that `b` that is defined inside of `a`_.

///

///

/// tab | Specific Import

### Importing specific Code Elements

As an alternative, we can also import a specific code element (variable, function …) by name.
In this case we also then don't need a specific prefix to use it.

```python
from constants import INDIVIDUALS_PER_GROUP

print(INDIVIDUALS_PER_GROUP)
```

///

In most cases, specific importing is to be preferred.
Import statements usually go to the top of a file to make clear from the beginning, which other modules are involved.

### Code Update

Let's use specific imports to get our code working again.

in our `__main__.py` file, we add the required import statements for all the variables we currently use:

```python
from constants import INDIVIDUALS_PER_GROUP, FOOD_PER_INDIVIDUAL
from parameters import starting_population, food_per_day

print("The Alien Growth Model")

current_population = starting_population
current_food = food_per_day  # Don't forget to feed them on the first day
excess_food = current_food - current_population * FOOD_PER_INDIVIDUAL
number_groups = current_population // INDIVIDUALS_PER_GROUP

print(current_population, "individuals,", current_food, "food, leaves", excess_food, "food")
print("There are", number_groups, "groups")
```

/// note | Key Points

* Splitting programs into dedicated files can be a good idea
* To access variables or functions defined in other files, they can be _imported_
* It is easier to tie a program together then to split it up.

///

/// details | Code Checkpoint

This is the code that we have so far:

* 📁 `alien_growth_model`
    * [`__main__.py`](../code_checkpoints/05-imports/__main__.py)
    * [`constants.py`](../code_checkpoints/05-imports/constants.py)
    * [`parameters.py`](../code_checkpoints/05-imports/parameters.py)

///

