from constants import INDIVIDUALS_PER_GROUP, FOOD_PER_INDIVIDUAL
from parameters import starting_population, food_per_day

print("The Alien Growth Model")

current_population = starting_population
current_food = food_per_day  # Don't forget to feed them on the first day
excess_food = current_food - current_population * FOOD_PER_INDIVIDUAL
number_groups = current_population // INDIVIDUALS_PER_GROUP

print(current_population, "individuals,", current_food, "food, leaves", excess_food, "food")
print("There are", number_groups, "groups")
