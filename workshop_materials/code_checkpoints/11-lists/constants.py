# Constant values, independent of starting parameters

# How many individuals are required to form a group
INDIVIDUALS_PER_GROUP = 7

# Number of individuals that will be born (per group) if excess food is available
GROWTH_PER_GROUP = 3

# Number of individuals that will perish (per group) if food is insufficient
DECLINE_PER_GROUP = 2

# Amount of food (in kg) that each individual requires
FOOD_PER_INDIVIDUAL = 0.25

# We consider the first day having the number 1
START_DAY = 1
