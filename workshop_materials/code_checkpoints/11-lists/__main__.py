from constants import START_DAY
from parameters import input_positive_integer
from simulation import simulate_day

print("The Alien Growth Model")

starting_population = input_positive_integer("starting population")
food_per_day = input_positive_integer("amount of food per day")
simulation_duration = input_positive_integer("simulation duration (in days)")

# Variables to carry information over from one day to the other
current_population = starting_population
current_food = food_per_day
population_over_time = [starting_population]

for current_day in range(START_DAY, START_DAY + simulation_duration):
    print("Population at the start of day:", current_population)
    print("Food at the start of day:", current_food)

    (current_population, current_food) = simulate_day(current_population, current_food)

    print("Population at the end of day:", current_population)
    print("Food at the end of day:", current_food)

    # Feed the aliens between the days
    current_food = current_food + food_per_day

    # Note down the population at the end of the day
    population_over_time.append(current_population)

# After the loop print all the gathered data as a summary
print("Population over time:", population_over_time)

# Calculate some statistical values
gathered_values = len(population_over_time)
lowest_population = min(population_over_time)
highest_population = max(population_over_time)
average_population = sum(population_over_time) / gathered_values

print("We gathered", gathered_values, "data points")
print("Minimum:", lowest_population, "individuals")
print("Maximum:", highest_population, "individuals")
print("Average:", average_population, "individuals")
