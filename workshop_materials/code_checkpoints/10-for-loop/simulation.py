from constants import INDIVIDUALS_PER_GROUP, FOOD_PER_INDIVIDUAL, GROWTH_PER_GROUP, DECLINE_PER_GROUP

def simulate_day(initial_population, initial_food):
    """Simulate the passing of one day.

    Don't forget to feed the aliens between two simulation steps!

    Args:
        initial_population:
            The population at the beginning of the day.
        initial_food:
            The amount of food available at the beginning of the day.
    Returns:
        A 2-element tuple containing the population at the end of the day 
        and the remaining food at the end of the day.
    """
    excess_food = initial_food - initial_population * FOOD_PER_INDIVIDUAL
    number_groups = initial_population // INDIVIDUALS_PER_GROUP

    will_grow = excess_food > 0
    will_shrink = excess_food < 0

    # Calculate the population at the end of the day
    if will_grow:
        new_population = initial_population + number_groups * GROWTH_PER_GROUP
    elif will_shrink:
        new_population = initial_population - number_groups * DECLINE_PER_GROUP
    else: 
        new_population = initial_population

    # Deal with negative excess food, since we can not carry over missing food to the next day
    if excess_food < 0:
        excess_food = 0

    return (new_population, excess_food)
