
def input_positive_integer(ask_for_what):
    """Query the user to input a positive number.

    If the given number was negative, the user is prompted to try again.

    Returns:
        A positive integer number as given by user input.
    """
    prompt = "Please input the " + ask_for_what + ": "
    input_is_valid = None
    while not input_is_valid:
        user_input = input(prompt)
        input_as_int = int(user_input)
        input_is_valid = input_as_int > 0
        if not input_is_valid:
            print("Sorry,", input_as_int, "is not a valid input, please try again")

    return input_as_int
