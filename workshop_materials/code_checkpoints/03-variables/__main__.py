
print("The Alien Growth Model")

# Constant values, independent of starting parameters

# How many individuals are required to form a group
INDIVIDUALS_PER_GROUP = 7

# Number of individuals that will be born (per group) if excess food is available
GROWTH_PER_GROUP = 3

# Number of individuals that will perish (per group) if food is insufficient
DECLINE_PER_GROUP = 2

# Amount of food (in kg) that each individual requires
FOOD_PER_INDIVIDUAL = 0.25

# Simulation starting parameters

# The amount of individuals at the begin of the simulation
starting_population = 118

# The amount of food available for each day
food_per_day = 20

current_population = starting_population
current_food = food_per_day  # Don't forget to feed them on the first day
excess_food = current_food - current_population * FOOD_PER_INDIVIDUAL
number_groups = current_population / INDIVIDUALS_PER_GROUP

print(current_population, "individuals,", current_food, "food, leaves", excess_food, "food")
print("There are", number_groups, "groups")
