# Simulation starting parameters

# The amount of individuals at the begin of the simulation

# We need a variable to check if we should enter the loop
# But there was no input yet, so can't say
input_is_valid = None  

while not input_is_valid:
    user_input = input("Please input the size of the starting population: ")
    input_as_int = int(user_input)
    input_is_valid = input_as_int > 0  # Re-evaluate the validity
    if not input_is_valid:
        print("Sorry,", input_as_int, "is not a valid input, please try again")

# We are now outside the loop (no longer indented)
# The input_as_int should be valid now
starting_population = input_as_int

# The amount of food available for each day
food_per_day = 20
