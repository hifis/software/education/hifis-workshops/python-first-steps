from constants import INDIVIDUALS_PER_GROUP, FOOD_PER_INDIVIDUAL, GROWTH_PER_GROUP, DECLINE_PER_GROUP
from parameters import starting_population, food_per_day

print("The Alien Growth Model")

current_population = starting_population
current_food = food_per_day  # Don't forget to feed them on the first day
excess_food = current_food - current_population * FOOD_PER_INDIVIDUAL
number_groups = current_population // INDIVIDUALS_PER_GROUP

print(current_population, "individuals,", current_food, "food, leaves", excess_food, "food")
print("There are", number_groups, "groups")

will_grow = excess_food > 0
will_shrink = excess_food < 0

if will_grow:
    current_population = current_population + number_groups * GROWTH_PER_GROUP
    print("Population grows to", current_population, "individuals")
elif will_shrink:
    current_population = current_population - number_groups * DECLINE_PER_GROUP
    print("Population shrinks to", current_population, "individuals")
else:
    print("Population is stable at", current_population, "individuals")
