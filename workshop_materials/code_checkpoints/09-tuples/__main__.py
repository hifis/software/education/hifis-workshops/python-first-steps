from parameters import input_positive_integer
from simulation import simulate_day

print("The Alien Growth Model")

starting_population = input_positive_integer("starting population")
food_per_day = input_positive_integer("amount of food per day")

# Variables to carry information over from one day to the other
current_population = starting_population
current_food = food_per_day

print("Population at the start of day:", current_population)
print("Food at the start of day:", current_food)

(current_population, current_food) = simulate_day(current_population, current_food)

print("Population at the end of day:", current_population)
print("Food at the end of day:", current_food)

# Feed the aliens between the days
current_food = current_food + food_per_day
