# Simulation starting parameters

# The amount of individuals at the begin of the simulation
starting_population = 118

# The amount of food available for each day
food_per_day = 20
