<!--
SPDX-FileCopyrightText: 2024 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# License

If not noted otherwise, text and images licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) and code under [MIT](https://opensource.org/license/mit).

A detailed overview about the copyright holders and licenses can be found in [the workshop materials repository]({{config.repo_url}}-/blob/main/LICENSE.md). 
