<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Code Structures - Functions

## Function Definitions

/// tab | Description

#### Use Case
Extract code into a separate segment to reduce complexity, and allow for re-usability.

Functions are separate sections of code that can be executed at will.
They have _parameters_ that describe the input values into a function and are used to control a functions' inner workings.
Further, functions may generate an output value that can be _returned_ to indicate for example a calculation result.

A function _definition_ only declares which _parameters_ and _return values_ there are and what code will be executed.
To actually run the code inside a function, it needs to be _called_. 

When a function modifies the programs' state directly (besides returning a value) it is said to have a _side effect_.
Side effects are usually considered bad practise, since they can make programs very confusing and hard to understand and debug.
Functions that rely only on their parameters for information input and have no side-effects are called _pure_ functions.

///

/// tab | Code Examples

#### Example: Function with no Parameters


This is a minimal example of a function that has

| Input Parameters | Return Value | Side effects | Pure function |
|------------------|--------------|--------------|---------------|
| —                | –            | –            | Yes           |

```python
# Function definition
def say_hello():
    print("Hello World")

# Function call
say_hello()  # No variable needed to catch return value
```

#### Example: Function with Parameters and Return Value

This is a more complex function that clamps a value between a minimum and maximum.

| Input Parameters | Return Value | Side effects | Pure function |
|------------------|--------------|--------------|---------------|
| value,           | clamped value| –            | Yes           |
| minimum,         |
| maximum          |

```python
# Function definition

def clamp(value, minimum, maximum):
    if value < minimum:
        return minimum
    elif value > maximum:
        return maximum
    else:
        return value

# Function call (positional arguments)
clamped_value = clamp(10, 0, 5)  # clamped_value will be 5

# Function call (named arguments)
clamped_value = clamp(minimum=0, maximum=5, value=10)  # clamped_value will be 5
```
///

### Function Call

/// warning | Don't forget the parenthesis

To call a function you write down its _name_ followed by parenthesis `(` and `)`.
If there are arguments for the function they are specified between the parenthesis.
But even if there are no arguments, the parenthesis **must** be there.

///

/// tab | Description

_Calling_ a function refers to its usage to execute a functions' code according to the _function definition_.

#### Use case

Employ a previously defined function to complete a more complex task.

A function may require additional information to complete its task.
When calling a function, these are referred to as _arguments_.
Some arguments are required, others may be optional or use a default value instead.

///

/// tab | Code Examples

#### Example 1

Call the function `print(…)` without any arguments
You still need the parenthesis `(`…`)` even if they are empty!

```python
print()
```

#### Example 2

Call the function `print(…)` with one argument.

```python
print("Hello World")
```

#### Example 3

Call the function `print(…)` with many arguments.
Multiple arguments are separated by a comma.

```python
print("Hello", "World", "how", "are", "you?")
```

#### Example 4

You can also use variables as arguments.

```python
my_text = "Hello World"
print(my_text)
```

#### Example 5

Arguments have names and may be referenced by them.
Here we use `--` as the word separator; The corresponding argument is named `sep`.
The names and meanings of the possible argument names are defined by the _function definition_.
You may want to check the functions' documentation for details.

```python
print("Hello", "World", sep="--")
```

Output:

```
Hello--World
```

///
