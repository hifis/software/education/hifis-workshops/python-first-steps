<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Code Structures in Python

## Introduction

Program code can be subdivided into several elements that fulfill specific purposes.
Those described in this reference are considered to be the _fundamental ones_, sufficient to solve any programming problem.
Many additional structural elements exist, mostly to enable the programmer to tackle very complex problems more easily and in a more convenient way.
Some of these elements come in variations, which usually depend on the use-case they are employed for.

## List of Code Structures

* [Assignments](./assignments.md)
* [Imports](./imports.md)
* [Conditionals](./conditionals.md)
* [Loops](./loops.md)
* [Functions](./functions.md)
