<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0 AND MIT
-->

# Code Structures - Imports

/// tab | Description

Importing elements from another _Python_ module makes them available in the current context (usually a script or a REPL-session).
This allows for code to be split into several segments to keep them invididually self-contained, reusable and maintainable.

#### Use case

Make a separate module or elements of a module available in the current context.

///

/// tab | Code Examples

#### Example: Importing a whole Module

In the following example, we import the `math`-module from _Python_s standard library and access the `sin(…)`-function and `pi` therein.

```python
import math

print(math.sin(math.pi))
```

#### Example: Importing Elements from a Module

In this example, only the `sin(…)`-function and `pi` will be imported and used.

```python
from math import sin, pi

print(sin(pi))
```

///
