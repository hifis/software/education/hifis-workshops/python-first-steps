<!--
SPDX-FileCopyrightText: 2024 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

## General process

- Please open **an issue**, if you have improvement ideas.
- Please open **a merge request**, if you want to share some improvements to the material.
- If you contribute content to the repository, please make sure to:
  - Mark your copyright (or the copyright of your research organization / company) using the SPDX `SPDX-FileCopyrightText` tag
  - Add yourself (once) to the list of contributors in the [README](README.md#contributors)

## Style guide

Here we list some main style rules when contributing to this project:

- Please define the file titles in the [mkdocy.yml] navigation section.
- Please use sentence case for section titles and captions.
- Please follow the one sentence, one line principle.
- Please use the new [block admonition syntax](https://facelessuser.github.io/pymdown-extensions/extensions/blocks/plugins/admonition/).
- Please use the new [block details syntax](https://facelessuser.github.io/pymdown-extensions/extensions/blocks/plugins/details/) as follows to indicate instructor notes:

```markdown
/// details | Instructor notes
	type: tip
Some content for workshop instructors only.
///
```
