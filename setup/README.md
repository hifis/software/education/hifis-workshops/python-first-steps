<!--
SPDX-FileCopyrightText: 2024 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Workshop setup

The workshop content is provided via a collaborative Markdown pad served via a [HedgeDoc instance](https://hedgedoc.org/).
This pad contains the schedule, the preparation information as well as the content of the different episodes for the specific workshop.

The default workshop content and schedule can be found in [default-pad.md](default-pad.md).
These adjustments stay locally and shall not be commited. 
