<!--
SPDX-FileCopyrightText: 2024 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# First Steps in Python
 
# Organizational Details

Here you can list the schedule and different preparation tasks.

Below you can further structure the content that you want to show during the workshop.
