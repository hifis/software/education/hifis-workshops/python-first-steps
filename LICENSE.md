# License

The copyright of this work is owned by different legal entities:
- The Helmholtz-Zentrum Dresden-Rossendorf (HZDR) and the German Aerospace Center created the workshop material template.
- 2022 - 2025 Helmholtz-Zentrum Dresden-Rossendorf created the actual workshop materials.

This work is licensed under multiple licenses:
- Episodes, documentation and web site content are licensed under [CC-BY-4.0](LICENSES/CC-BY-4.0.txt).
- Insignificant files and configuration files are licensed under [CC0-1.0](LICENSES/CC0-1.0.txt).
- Code is licensed under [MIT](LICENSES/MIT.txt).

Please see the individual files for detailed copyright and license information.
This information is provided in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
